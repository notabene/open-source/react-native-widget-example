export const getCustomerToken = async (
	apiUrl: string,
	apiToken: string,
	vaspDID: string
) => {
	try {
		const res = await fetch(apiUrl + "/auth/customerToken", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: "Bearer " + apiToken,
			},
			body: JSON.stringify({
				vaspDID: vaspDID,
				customerRef: "customerRef",
			}),
		});
		const data = await res.json();
		return data.access_token;
	} catch (error) {
		alert(error);
	}
};
