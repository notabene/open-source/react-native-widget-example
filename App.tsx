import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View } from "react-native";
import WebView from "react-native-webview";
import { readAsStringAsync } from "expo-file-system";
import { useAssets } from "expo-asset";
import { useEffect, useState } from "react";
import Constants from "expo-constants";
import { API_TOKEN, WIDGET_BASE, API_URL, VASP_DID } from "@env";
import { getCustomerToken } from "./utils";

// Script must be loaded inside the webview
const init = () => {
	try {
		function addScript(src: string) {
			var s = document.createElement("script");
			s.setAttribute("src", src);
			document.body.appendChild(s);
			return s;
		}

		const script = addScript(
			"https://unpkg.com/@notabene/javascript-sdk@latest/dist/es/index.js"
		);

		script.onload = () => {
			const nb = new Notabene({
				vaspDID: VASP_DID,
				widget: WIDGET_BASE,
				container: "#container",
				authToken: "CUSTOMER_TOKEN_REPLACE_ME",
				onValidStateChange: (isValid: string) => {
					console.log(isValid);
					window.ReactNativeWebView.postMessage(JSON.stringify(nb.tx));
				},
			});
			nb.setTransaction({
				transactionAsset: "ETH",
				beneficiaryAccountNumber: "0x6F3970f0b88B7cc706cB493Bb385585a511123444",
				transactionAmount: "2000",
			});
			nb.renderWidget();
		};
	} catch (error) {
		alert(error);
	}
};

export default function App() {
	const [index, indexLoadingError] = useAssets(require("./widget.html"));
	const [tx, setTx] = useState({});
	const [customerToken, setCustomerToken] = useState();

	const [html, setHtml] = useState("");

	useEffect(() => {
		(async () => {
			if (!API_TOKEN) {
				alert("API_TOKEN is not set");
				return;
			}
			const token = await getCustomerToken(API_URL, API_TOKEN, VASP_DID);
			setCustomerToken(token);
		})();
	}, []);

	if (index) {
		readAsStringAsync(index[0].localUri as string).then((data) => {
			setHtml(data);
		});
	}

	if (!customerToken || !html) return <Text>Loading...</Text>;

	const initScript = `(${String(init)})()`.replace(
		"CUSTOMER_TOKEN_REPLACE_ME",
		`${customerToken}`
	);

	return (
		<View style={styles.container}>
			<StatusBar style="auto" />
			<View
				style={{
					top: Constants.statusBarHeight,
					display: "flex",
					flexDirection: "column",
					width: "100%",
					height: "100%",
				}}
			>
				{tx.isValid && (
					<Text style={{ fontSize: 10 }}>{JSON.stringify(tx, null, 2)}</Text>
				)}
				<WebView
					containerStyle={{
						width: "100%",
						height: "100%",
						paddingTop: 10,
						paddingLeft: 5,
						paddingRight: 5,
						backgroundColor: "lightgreen",
					}}
					useSharedProcessPool={true}
					// Load the script inside the webview
					injectedJavaScript={initScript}
					allowFileAccess
					allowingReadAccessToURL="*"
					allowUniversalAccessFromFileURLs={true}
					source={{ html, baseUrl: WIDGET_BASE }}
					originWhitelist={["*"]}
					onMessage={(event) => {
						setTx(JSON.parse(event.nativeEvent.data));
					}}
					javaScriptCanOpenWindowsAutomatically
					mixedContentMode="always"
					onError={(syntheticEvent) => {
						const { nativeEvent } = syntheticEvent;
						console.warn("WebView error: ", nativeEvent);
					}}
				/>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff",
		alignItems: "center",
		justifyContent: "center",
	},
});
