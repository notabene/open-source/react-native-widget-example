## React Native Widget Example

This is a simple example using Expo: https://expo.dev/ and `react-native-webview`

<div>
    <img src='./widget.gif' width='250px'>
</div>

### usage

Install dependencies

```
npm install
```

or

```
yarn
```

Create a `.env` file (copy from `.env-template`) and fill the missing variables

```
WIDGET_BASE=https://beta-widget-demo.notabene.dev
API_URL=https://api-demo.notabene.dev
VASP_DID={VASP DID}
API_TOKEN={API TOKEN}
```

---

Run the project in the Expo app

```
yarn start
```

(for resetting cache when changing environment variables)

```
yarn start-no-cache
```

You need to run the project using a device or emulator (ios or android).
